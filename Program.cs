﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4
{
    class Program
{
    static void Main(string[] args)
    {
        Dataset csvcsv = new Dataset(@"C:\Users\Marko\Desktop\datoteka.csv");
        Adapter adapter = new Adapter(new Analyzer3rdParty());
        double[][] matrix = adapter.ConvertData(csvcsv);

        foreach (double d in adapter.CalculateAveragePerColumn(csvcsv))
        {
            Console.WriteLine(d);
        }

        Adapter.ToString(matrix);
    }
}
}

